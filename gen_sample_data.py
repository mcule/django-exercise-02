import argparse
import csv
import datetime
import logging
import random
import string
import sys
from collections import defaultdict

from faker import Faker


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
ch.setLevel(logging.INFO)
logger.addHandler(ch)


def gen(num_suppliers, num_products, num_ordered_products, random_seed=0):
    random.seed(random_seed)
    fake = Faker()
    Faker.seed(random_seed)

    suppliers = [(fake.company(), i + 1) for i in range(num_suppliers)]
    logger.info('suppliers: %s', suppliers)

    purity_choices = [None, 85, 90, 95, 99]
    supplier_counts = {s: 0 for s, __ in suppliers}
    products = []
    for i in range(1, num_products + 1):
        supplier, supplier_idx = random.choices(suppliers, weights=range(1, num_suppliers + 1))[0]
        supplier_counts[supplier] += 1
        supplier_ref_id = fake.bothify(
            text='??-########',
            letters=string.ascii_uppercase
        )
        p = {
            'ref_id': f'P-{i}',
            'supplier': supplier,
            'supplier_idx': supplier_idx,
            'supplier_ref_id': supplier_ref_id,
            'purity': random.choice(purity_choices),
            'price': round((random.randint(1, 10) + 0.99) * supplier_idx, 2),
            'price_currency': random.choices(['USD', 'EUR', 'GBP'], [100, 25, 5])[0],
        }
        products.append(p)

    logger.info('products sample: %s', products[:5])
    logger.info('suppliers counts: %s', supplier_counts)

    amounts = [1, 2, 5, 10, 20]
    product_counts = defaultdict(int)

    fieldnames = [
        'ref_id',
        'supplier',
        'supplier_ref_id',
        'purity',
        'amount_mg',
        'price',
        'price_currency',
        'ordered_on',
        'delivered_on',
        'is_canceled',
    ]
    writer = csv.DictWriter(sys.stdout, fieldnames=fieldnames)
    writer.writeheader()
    for i in range(num_ordered_products):
        product = random.choice(products).copy()
        supplier_idx = product.pop('supplier_idx')
        product_counts[product['ref_id']] += 1
        amount_mg = random.choices(amounts, range(len(amounts), 0, -1))[0]
        is_canceled = random.choices(
            [0, 1],
            [num_ordered_products, num_ordered_products * 0.01]
        )[0]
        start_date = datetime.date(2020, 1, 1)
        end_date = datetime.date(2021, 12, 31)
        ordered_on = fake.date_between_dates(start_date, end_date)
        if is_canceled or (end_date - ordered_on).days < 5:
            delivered_on = None
        else:
            delivered_on = ordered_on + datetime.timedelta(
                days=random.randint(2, 10 + supplier_idx)
            )
        price = min(9999.99, round(product['price'] * amount_mg, 2))
        row = product
        row.update({
            'price': price,
            'amount_mg': amount_mg,
            'ordered_on': ordered_on,
            'delivered_on': delivered_on,
            'is_canceled': is_canceled,
        })
        writer.writerow(row)

    product_counts_sorted = sorted(
        product_counts.items(),
        key=lambda x: x[1],
        reverse=False
    )
    logger.info('product counts sample: %s', product_counts_sorted[:100])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        '--num-suppliers',
        type=int,
        default=20,
        help="The number of suppliers to generate.",
    )
    parser.add_argument(
        '--num-products',
        type=int,
        default=2000,
        help="The number of products to generate."
    )
    parser.add_argument(
        '--num-ordered-products',
        type=int,
        default=100000,
        help="The number of ordered products to generate."
    )
    parser.add_argument(
        '--random-seed',
        type=int,
        default=0,
        help="Explicit random seed value."
    )
    args = parser.parse_args()

    gen(
        num_suppliers=args.num_suppliers,
        num_products=args.num_products,
        num_ordered_products=args.num_ordered_products,
        random_seed=args.random_seed,
    )
