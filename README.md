# Setup

As first steps:

* Clone this Git repository to your computer.
* Create a Python3 virtualenv and activate it.
* Install requirements with: `pip install -r requirements.txt`
* Create a [Django](https://www.djangoproject.com/) project with: `django-admin startproject ordering`

At this point your file tree should look like this:

```
├── README.md
├── data.csv
├── gen_sample_data.py
├── ordering
│   ├── manage.py
│   └── ordering
│       ├── __init__.py
│       ├── asgi.py
│       ├── settings.py
│       ├── urls.py
│       └── wsgi.py
└── requirements.txt
```

You can find `data.csv` and `gen_sample_data.py` files in the root directory.

The `data.csv` CSV file contains a very simplified sample data of a company's internal ordering system. Namely the orders the company sends to its suppliers. You need to use this file as an input later. It was generated with this command:

```
python gen_sample_data.py > data.csv
```

Feel free to experiment with `gen_sample_data.py` to generate a different sample data (maybe a smaller set) for testing purposes. You can check `python gen_sample_data.py --help` for the available options.


The `data.csv` contains the following fields:


* `ref_id`: The reference ID of the product that has been ordered. You can assume that this is always specified (non-empty) and it is always the same for the same product.

> Note: A product can appear multiple times in the input depending on how many times it has been ordered (ordered product).

* `supplier`: The name of the product's supplier. You can assume that this is always specified and it is always the same for the same product.
* `supplier_ref_id`: You can assume that this is always specified and it is always the same for the same product.
* `purity`: The purity percentage of the product. It is always an integer if specified but it might be missing (empty) for some products. It is always the same for the same product.
* `amount_mg`: You can assume that this is always specified, it is always an integer and always less than 100.
* `price`: You can assume that this is always specified and always a number.
* `price_currency`: You can assume that this is always specified and the values can be USD, EUR, GBP only.
* `ordered_on`: It is a ISO 8601 date value. You can assume that it is always specified.
* `delivered_on`: It is a ISO 8601 date value. It might be missing (empty).
* `is_canceled`: Either 0 (false) or 1 (true).


# Tasks

## Create models

You should create 3 applications in your Django project:

**1. supplier**

Containing a model called `Supplier` with fields:

* `name`: related input data column: `supplier`

**2. product**

Containing a model called `Product` with fields:

* `ref_id`: Related input data column: `ref_id`
* `supplier`: Many-to-one relationship with the `Supplier` model.
* `supplier_ref_id`: Related input data column: `supplier_ref_id`.
* `purity`: The field supposed to store values between 0 and 100 since it is a percentage value. Choose the most suitable model field. Related input data column: `purity`
* `price`: 1mg price of the product. The input data doesn't contain this value directly, you need to calculate it. The pricing of the products is linear so if 10mg of a product costs 20 USD then the 1mg price of the product is 2 USD, 2mg price is 4 USD etc. Use a DecimalField for this with max_digits=6 and decimal_places=2. You can assume that this is always present in the input.
* `price_currency`: Related input data column: `price_currency`
* `price_usd`: 1mg price of the product in USD. The input data doesn't contain this value directly, you need to calculate it according to the exchange rate data specified below. Use a DecimalField for this with max_digits=6 and decimal_places=2.

**3. ordered_product**

Containing a model called `OrderedProduct` with fields:

* `product`: Many-to-one relationship with the `Product` model.
* `price`: Use a DecimalField for this with max_digits=6 and decimal_places=2. Related input data column: `price`
* `price_currency`: Related input data column: `price_currency`
* `price_usd`: Price in USD. The input data doesn't contain this value directly, you need to calculate it according to the exchange rate data specified below. Use a DecimalField for this with max_digits=6 and decimal_places=2.
* `amount_mg`: Related input data column: `amount_mg`
* `ordered_on`: Date field. Related input data column: `ordered_on`
* `delivered_on`: Date field. Related input data column: `delivered_on`
* `is_canceled`: Boolean field. Related input data column: `is_canceled`

All models can use Django's default auto-incrementing primary key as the `id` field.

For "missing" values you should use `None` or empty strings.

You can use this exchange rate data for currency conversions. We let you decide how you use this information in the  code:

```
1 EUR = 1.168091 USD
1 GBP = 1.367285 USD
```

Define sensible `__str__` methods for your models.

You can use SQLite for the database.

---

## Write a management command

Write a management command in the `ordered_product` application called `load_ordered_products` which iterates over the input entries and load them into the database. After loading the data your DB should contain 20 suppliers, 2000 products and 100000 ordered products with the above specified fields filled with the appropriate values. Products must have the right relation with suppliers, ordered products must have the right relation with products.

---

## Setup admin site

Django's admin interface must be enabled and available on the default `/admin/` URL.

All the models you create must have an appropriate ModelAdmin specified in `admin.py` files of the applications.

Use `raw_id_fields` for the relationship fields.

---

## Create API

Using [Django REST framework](https://www.django-rest-framework.org/)

* Create API endpoints where suppliers, products, ordered products can be listed.
* You should use Django REST Framework views, serializers.
* You should enable pagination with a sensible page size.
* The list endpoints should support ordering and basic filtering on direct fields. By basic, we mean that it doesn't have to support range, gt, gte, lt, lte, contains and similar filters, only equality. Additionally, the ordered products list endpoint should also support filtering/ordering by a supplier (besides the direct fields).
* Also provide detail endpoints where an individual supplier, product, ordered product can be retrieved.
* Example supplier entry in both the list and detail response bodies:
```
{
    "id": 1,
    "name": "Patrick, Barrera and Collins"
}
```
* A product entry in the response should contain the supplier as a nested field. Make sure you optimize the DB queries to support this in an efficient way, try to avoid the N + 1 queries problem. Example product entry in both the list and detail response bodies:
```
{
    "id": 1,
    "price": "1.99",
    "price_currency": "USD",
    "price_usd": "1.99",
    "purity": 95,
    "ref_id": "P-111",
    "supplier": {
        "id": 1,
        "name": "Patrick, Barrera and Collins"
    },
    "supplier_ref_id": "MB-14296354"
}
```
* An ordered product entry in the response should contain the product as a nested field (which contains the supplier as a nested field). Make sure you optimize the DB queries to support this in an efficient way, try to avoid the N + 1 queries problem. Example ordered product entry in both the list and detail response bodies:
```
{
    "amount_mg": 20,
    "delivered_on": "2021-01-15",
    "id": 1,
    "is_canceled": false,
    "ordered_on": "2021-01-06",
    "price": "39.80",
    "price_currency": "USD",
    "price_usd": "39.80",
    "product": {
        "id": 1,
        "price": "1.99",
        "price_currency": "USD",
        "price_usd": "1.99",
        "purity": 95,
        "ref_id": "P-111",
        "supplier": {
            "id": 1,
            "name": "Patrick, Barrera and Collins"
        },
        "supplier_ref_id": "MB-14296354"
    }
}
```

* Create an endpoint where the following statistics can be retrieved of ordered products:
    * Distribution of the total number, total price (in USD), average price (in USD) of ordered products among the suppliers.
    * Distribution of the total number, total price (in USD), average price (in USD) of ordered products among all the different months. A month is determined by the year and the month of the `ordered_on` date field.
    * The fastest 5 suppliers based on their average delivery time. The delivery time value of an ordered product can be calculated by `delivered_on - ordered_on`.
* The statistics endpoint should support the same filtering options as the endpoint for listing ordered products.
* You should produce the required statistics values by issuing the right DB queries using the Django ORM. You can not fetch the entire data and calculate the statistics on Python level, make the DB do this job for you.
* Use a sensible data structure in the statistics response, we don't give you exact instructions on how it should look like. You can represent suppliers with only their ID in the response.

## Write shell scripts

Write two Linux shell/bash scripts. They should be placed in the root directory (next to `gen_sample_data.py`).

1. `clean_code.sh`: It should remove `__pycache__` folders, and files with `.pyc` extension from the entire directory tree.
2. `count_lines.sh`: It should count the non-empty lines of all `.py` files in the entire directory tree and produce an output where the first column is the non-empty lines count of the file, the second is the file's relative path. Exclude the files that have zero non-empty lines. The output has to be ordered by the first column in ascending order.

```
...
18 ./ordering/manage.py
...
128 ./gen_sample_data.py
```

---

After you are done with your work, please use the above mentioned `clean_code.sh` first, then zip the whole thing together in a single file and send us back. We'll check it asap.
Thank you!
